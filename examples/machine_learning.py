### Computation of a summary table on important features that distinguish between two datasets found using machine learning tools

import pandas as pd
import logging
logging.basicConfig(filename='log_ml.txt',level=logging.INFO)
import seaborn as sns
import os

from chgen.wrapper import classify

# prepare dataframes
df = sns.load_dataset('diamonds')

df_1 = df[df.index < 500].copy()
df_2 = df[(df.index >= 500) & (df.index < 1000)].copy().reset_index(drop = True)

# remove categorical data
df_1.drop(columns = ['color', 'clarity', 'cut'], inplace = True)
df_2.drop(columns = ['color', 'clarity', 'cut'], inplace = True)

# label dataframes for classification problem solution
df_1['df_name'] = 'first_df'
df_2['df_name'] = 'second_df'

# target: dependent variable
# df_name: name of a dataframe (in target variable) to be set at 1 in the classification problem
# corr_coef: float, coefficient of acceptable correlation between features. If more, then one of the correlated features will be dropped
result = classify(df_1 = df_1, df_2 = df_2, target = 'df_name', df_name = 'first_df', corr_coef = 0.9)
os.makedirs('./result_diamonds', exist_ok = True)
result.to_csv('./result_diamonds/Classification.csv', index=False)
