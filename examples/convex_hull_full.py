### An example of intersection values computation for the case when datasets are taken at their full size

import numpy as np
import pandas as pd
import random
import os
from itertools import combinations, repeat
from pathos.multiprocessing import ProcessingPool as Pool, cpu_count
import seaborn as sns
import multiprocessing
import logging
logging.basicConfig(filename='log_ch.txt',level=logging.INFO)

from chgen.wrapper import find_loss, compute_intersections

# define a dimension (number of features in a combination) and names to the dataframes
# that are used for calculations. The same names must be used for intersection value
# analysis in further_analysis.py script
dim = 2
df_names = ['first_df', 'second_df']
logging.info(f'CURRENT PAIR: {df_names[0]} and {df_names[1]}')
logging.info(f'CURRENT DIMENSION: {dim}')

# prepare example dataframes and their features
df = sns.load_dataset('diamonds')

df_1 = df[df.index < 1000].copy()
df_2 = df[(df.index >= 1000) & (df.index < 2000)].copy().reset_index(drop = True)
# remove categorical data
df_1.drop(columns = ['color', 'clarity', 'cut'], inplace = True)
df_2.drop(columns = ['color', 'clarity', 'cut'], inplace = True)

# find common parameters for both dataframes
common_params = sorted(list(set(df_1.columns).intersection(df_2.columns)))
logging.info(f'Number of common parameters: {len(common_params)} for {df_names[0]} and {df_names[1]}')
logging.info(f'Common parameters for {df_names[0]} and {df_names[1]}: {common_params}')

df_1 = df_1[common_params]
df_2 = df_2[common_params]

# create a list with all combinations of parameters for a defined dimension
combos = list(combinations(common_params, dim))
logging.info(f'Number of combos: {len(combos)} for {df_names[0]} and {df_names[1]}')

# store the results
os.makedirs('./result_diamonds', exist_ok = True)

# find outliers
eps = 0.5 #neighborhood distance of a point
min_samples = 4 #minimal number of neighbors of a point
not_noise = 0.9 #amount of points that form a cluster, i.e. it is allowed to have 10% of ouliers at most

# first dataset
result_bools_1 = [] # container for a point identification, True - part of a cluster, False - an outlier
df_loss = pd.DataFrame(columns = ['Combination','Loss','end_eps','end_sample'])
idx = 0
for combo in combos:
    bools, curr_combo, loss, end_eps, end_sample = find_loss(combo, df_1, df_names[0], eps, min_samples, not_noise)
    result_bools_1.append(bools)
    df_loss.loc[idx, 'Combination'] = curr_combo
    df_loss.loc[idx, 'Loss'] = loss
    df_loss.loc[idx, 'end_eps'] = end_eps
    df_loss.loc[idx, 'end_sample'] = end_sample
    idx += 1
df_loss.to_csv(f'./result_diamonds/end_eps_for_{df_names[0]}_in_pair_w_{df_names[1]}_full.csv', index=False)


# second dataset
result_bools_2 = []
df_loss = pd.DataFrame(columns = ['Combination','Loss','end_eps','end_sample'])
idx = 0
for combo in combos:
    bools, curr_combo, loss, end_eps, end_sample = find_loss(combo, df_2, df_names[1], eps, min_samples, not_noise)
    result_bools_2.append(bools)
    df_loss.loc[idx, 'Combination'] = curr_combo
    df_loss.loc[idx, 'Loss'] = loss
    df_loss.loc[idx, 'end_eps'] = end_eps
    df_loss.loc[idx, 'end_sample'] = end_sample
    idx += 1
df_loss.to_csv(f'./result_diamonds/end_eps_for_{df_names[1]}_in_pair_w_{df_names[0]}_full.csv', index=False)

# computation of intersection values with all points (outliers included) without sampling of instances
logging.info(f'BEFORE DBSCAN, {df_names[1]} is fixed')
df_1_in_2 = pd.DataFrame()
for combo in combos:
    df_curr = compute_intersections(combo = combo, df_1 = df_1, df_2 = df_2, df_names = df_names,
                                    bools_1 = [], bools_2 = [], first_fixed = None)
    df_1_in_2 = pd.concat([df_1_in_2, df_curr])
df_1_in_2.to_csv(f"./result_diamonds/full_{df_names[0]}_and_full_{df_names[1]}_all_points.csv", index=False)

# computation of intersection values after outlier removal without sampling of instances
logging.info(f'AFTER DBSCAN, {df_names[1]} is fixed')
df_1_in_2 = pd.DataFrame()
for combo, bool_1, bool_2 in zip(combos, result_bools_1, result_bools_2):
    df_curr = compute_intersections(combo = combo, df_1 = df_1, df_2 = df_2, df_names = df_names,
                                    bools_1 = bool_1, bools_2 = bool_2, first_fixed = None)
    df_1_in_2 = pd.concat([df_1_in_2, df_curr])
df_1_in_2.to_csv(f"./result_diamonds/full_{df_names[0]}_and_full_{df_names[1]}_wo_outliers.csv", index=False)
