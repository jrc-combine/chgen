### Examples of possible anaysis of the results obtained from intersection values calculation
### Here the tables with the unsampled results (i.e. the datasets were used at their full sizes) are used

import numpy as np
import pandas as pd
import scipy.stats as stats
import logging
logging.basicConfig(filename='log_analysis.txt',level=logging.INFO)

from chgen.analysis import fishers_exact_test, produce_summary_tables_ch

# define the names of datasets. Must match those used for intersection values calculation in convex_hull_full.py
df_pairs = ['first_df', 'second_df']
count = 7 # number of common parameters. The number can be found in log_ch.txt
path_to_ch_outputs = f'./result_diamonds/full_*_wo_outliers.csv' #FIXME: fix to the path where the tables are stored

# Approach 1 (suitable only for 2-dimensional case):
# Aggregate a table by the case (which dataset's convex hull is used for coverage) and a combination. Based on the grouped
# intersection values the combination of features with the smallest intersection values are found. Condition for the search is
# the lower fence, values that are smaller are considered as those that may include important features (i.e. those that
# discriminate between the sets). If the lower fence is negative, no such combinations are found
df = pd.read_csv(f'./result_diamonds/full_{df_pairs[0]}_and_full_{df_pairs[1]}_wo_outliers.csv') #FIXME: fix to the path where the table is stored
df_mean = df.groupby(['Name', 'Parameters']).mean()
df_mean = df_mean.reset_index()

# perform fisher's exact test. The test is performed without p-value correction
enrich_1 = fishers_exact_test(df_mean, f'full {df_pairs[0]} in full {df_pairs[1]}', count)
if not enrich_1.empty:
    enrich_1 = enrich_1[enrich_1['p_value'] < 0.05] # filter features based on the significance level. Here as an example 5%

#symmetric case
enrich_2 = fishers_exact_test(df_mean, f'full {df_pairs[1]} in full {df_pairs[0]}', count)
if not enrich_2.empty:
    enrich_2 = enrich_2[enrich_2['p_value'] < 0.05]

enrich_df = pd.concat([enrich_1, enrich_2]).reset_index(drop = True)
if not enrich_df.empty:
    # if the table is not empty it contains the list of important features, their count in 'bad' combinations (those with low
    # intersection values) and 'good' combinations (those that are higher than the lower fence), odds and p-values
    enrich_df.to_csv(f'./result_diamonds/important_parameters_{df_pairs[0]}_and_{df_pairs[1]}.csv', index=False)
else:
    logging.info('Bad parameters not found')

# Approach 2:
# Compared to the Approach 1, this function first extracts features out of the combinations and assigns an intersection value
# of a combination to its feature-component. Then computes the median value for each parameter and based on these median values
# calculates the lower fence. If a median values of a feature is smaller than the lower fence, this feature is considered important.
# The result of the function is three tables that describe the mean intersection values per a pair of datasets, lists important features
# that are important for the discrimination and their count. Rows - initial dataset, columns - dataset, convex hull of which is
# covered by the convex hull of the initial dataset
x,y,z = produce_summary_tables_ch(path_to_ch_outputs, df_pairs)
x.to_csv(f'./result_diamonds/mean_coverage.csv')
y.to_csv(f'./result_diamonds/important_parameters.csv')
z.to_csv(f'./result_diamonds/important_parameters_count.csv')
