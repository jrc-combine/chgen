# chgen

Convex Hull Generalization

## Installation
```
pip install chgen
```
Python >= 3.7 is required

<!--- ## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.rwth-aachen.de/jrc-combine/chgen.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://git.rwth-aachen.de/jrc-combine/chgen/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.
-->

## Description

#### Convex Hull Computation
This package performs convex hull analysis on two different datasets. It is investigated how many instances of one dataset is covered by the convex hull of another dataset. The fraction of covered points to the whole number of points contained in the dataset is called an intersection value. The user can define the dimension for comparison. For example, 2-dimensional comparison means that the set of two feature combinations is taken. For each combination, an intersection value is calculated 100 times. The calculation of intersection values can be performed on all points of the original dataset as well as on points after the outlier removal. 

Outlier detection is based on the DBSCAN algorithm, thus such parameters as *epsilon* and *min_samples* can be set. Each dataset is considered one cluster. The relative number of outliers in percentage can be defined by a user. The output table contains the information about the end number of detected outliers, epsilon and min_samples parameters per combination.

Additionally, it is possible to alter the size of one dataset to investigate how intersection values change depending on the size of the dataset. The output table contains information about intersection values per combination and the case of calculation (i.e. the convex hull of which dataset covers the points of another dataset).

It is considered that the combinations that resulted in the lowest intersection values contain features that can discriminate between the datasets.

#### Machine Learning Tools
Features that can distinguish between the datasets can be also found using standart machine learning tools, such as logistic regression, random forest, support vector machines and Adaboost classifiers. In this part, a classifier is trained on one dataset and tested on another. Performing two methods, feature omission and feature permutation, a user can obtain parameters that discriminate between the datasets and compare them to those found using convex hull analysis.

<!---## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
-->

## Usage
The *example*-folder constains the example scripts of how the package can be used. To run the further_analysis.py script, a user must run the convex_hull_full.py script first.

<!---
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->

## Authors and acknowledgment
Konstantin Sharafutdinov, Kateryna Nikulina

## License
MIT License

<!---
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
